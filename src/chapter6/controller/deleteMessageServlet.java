package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })

public class DeleteMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost (HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		//青の"deleteId"がjspで定義しているname=変数名
		String delete = request.getParameter("messageId");
		int messageId = Integer.parseInt(delete);
		new MessageService().delete(messageId);

		request.setAttribute("messageId", messageId);
		response.sendRedirect("./");
	}
}
