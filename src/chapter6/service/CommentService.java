package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import chapter6.beans.Comment;
import chapter6.dao.CommentDao;
import chapter6.dao.UserCommentDao;

public class CommentService {

	//返信を投稿する
	public void insert(Comment message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new CommentDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//返信したつぶやきを表示する
	public List<Comment> select() {

		Connection connection = null;

		try {
			connection = getConnection();

			List<Comment> comments = new UserCommentDao().select(connection);
			commit(connection);

			return comments;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
