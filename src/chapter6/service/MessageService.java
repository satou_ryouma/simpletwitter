package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

//つぶやきの投稿
public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//つぶやきを全て表示
	public List<UserMessage> select(String userId , String start, String end) {
		final int LIMIT_NUM = 1000;
		//日付を取得
		Date date = new Date();
		String dateNow = DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss");

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			//つぶやきの絞り込み
			String startDate = null;

			if(!StringUtils.isEmpty(start)) {
				startDate = start + " 00:00:00";
			}else {
				startDate = "2020-01-01 00:00:00";
			}

			String endtDate = null;
			if(!StringUtils.isEmpty(end)) {
				endtDate = end + " 23:59:59";
			}else {
				endtDate =  dateNow;
			}

			//特定のユーザーの全つぶやき表示できるよう追加
			List<UserMessage> messages = new UserMessageDao().select(connection, id, startDate, endtDate,  LIMIT_NUM);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//つぶやきの削除
	public void delete(int messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, messageId);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//編集したいつぶやきを表示
	public Message choice(int messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			Message message = new MessageDao().choice(connection, messageId);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//つぶやきの編集
	public void edit(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao(). edit(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
